import random

class Card():
    def __init__(self, name, suit):
        self.name = name
        self.suit = suit
    def __repr__(self):
        return self.name + ' ' + self.suit

#Подсчет "силы" карточного набора
def deck_rating(deck, trump):
    #"Сила" карт оценивается согласно данным из словаря rating_points
    rating_points = {
        '6': 6,
        '7': 7,
        '8': 8,
        '9': 9,
        '10': 10,
        'валет': 11,
        'дама': 12,
        'король': 13,
        'туз': 14
    }
    #Если масть карты совпадает с козырной, то она считается "сильнее" на points_for_trump очков
    points_for_trump = 9
    #В sum_for_deck находится общее количество очков для всего набора
    sum_for_deck = 0
    for card in deck:
        sum_for_deck = sum_for_deck + rating_points[card.name]
        if trump == card.suit:
            sum_for_deck = sum_for_deck + points_for_trump
    return sum_for_deck

#В списке pack находится вся игральная колода (36 карт)
pack = [Card('6', 'черви'), Card('6', 'бубны'), Card('6', 'трефы'), Card('6', 'пики'),
        Card('7', 'черви'), Card('7', 'бубны'), Card('7', 'трефы'), Card('7', 'пики'),
        Card('8', 'черви'), Card('8', 'бубны'), Card('8', 'трефы'), Card('8', 'пики'),
        Card('9', 'черви'), Card('9', 'бубны'), Card('9', 'трефы'), Card('9', 'пики'),
        Card('10', 'черви'), Card('10', 'бубны'), Card('10', 'трефы'), Card('10', 'пики'),
        Card('валет', 'черви'), Card('валет', 'бубны'), Card('валет', 'трефы'), Card('валет', 'пики'),
        Card('дама', 'черви'), Card('дама', 'бубны'), Card('дама', 'трефы'), Card('дама', 'пики'),
        Card('король', 'черви'), Card('король', 'бубны'), Card('король', 'трефы'), Card('король', 'пики'),
        Card('туз', 'черви'), Card('туз', 'бубны'), Card('туз', 'трефы'), Card('туз', 'пики')]
random.shuffle(pack) #Карты перетасовуются
trump = random.choice(pack).suit #Из колоды случайным образом выбирается карта, масть которой становится козырной
print('Козырь:', trump)
n = 0 #Количество игроков (наборов карт)
while(n < 2 or n > 6):
    n = int(input('Введите количество игроков (от 2 до 6):\n'))
сards_in_deck = 6 #Количество карт в одном наборе
#Словарь decks содержит данные в виде: {"сила" набора: набор в виде списка}
decks = dict()
for i in range(0, n*сards_in_deck, сards_in_deck):
    #Первый набор создается путем взятия первых сards_in_deck карт в колоде,
    #следующий, путем взятия следующих сards_in_deck карт и так далее
    deck = pack[i:i+сards_in_deck]
    decks[deck_rating(deck, trump)] = deck
print('Наборы карт:')
print(*decks.values(), sep='\n')
print('Самый "сильный" набор:', decks[max(decks.keys())], sep='\n')